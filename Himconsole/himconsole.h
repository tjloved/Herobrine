// Copyright 2019 SMS
// License(GPL)
// Author: ShenMian

#ifndef HIMCONSOLE_H_
#define HIMCONSOLE_H_

#include "type.h"

const auto LIST_NULL = -1;

class Himconsole
{
public:
	Himconsole();
	~Himconsole();

	void console();

	// 内置命令
	void banner();
	void clear();
	void exit();
	void help();
	void history();
	void ls();
	void use();

private:
	vector<string> arg_;					// 命令参数
	string         prompt_;				// 命令提示符

	struct cmd_info_
	{
		string name;
		string desc;
	};
	vector<struct cmd_info_> list;// 命令列表

	ushort        history_size_;	// 命令历史记录数
	deque<string> history_;				// 命令历史记录
};

#endif	// HIMCONSOLE_H_
