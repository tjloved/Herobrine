// Copyright 2019 SMS
// License(GPL)
// Author: ShenMian
// ���ض�

#include "slave.h"

uint Slave::lastId_	= 0;
uint Slave::size_		= 0;


Slave::Slave()
{
	size_++;

	id_ = lastId_;
	lastId_++;
}

Slave::~Slave()
{
	size_--;
}



uint Slave::size()
{
	return size_;
}


uint Slave::lastId()
{
	return lastId_;
}


uint Slave::id()
{
	return id_;
}

void Slave::os_info(string& os_info)
{
	memcpy(&os_info_, os_info.c_str(), os_info.size());
}
