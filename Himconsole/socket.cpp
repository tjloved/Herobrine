// Copyright 2019 SMS
// License(GPL)
// Author: ShenMian

#include "socket.h"

constexpr auto socket_mss = 512;	// 最大数据分段
#define buffer_file_size 2048		// 文件缓冲区大小



Socket::Socket()
{
	sock_                 = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	addr_.sin_family      = AF_INET;
	addr_.sin_addr.s_addr = 0;
	addr_.sin_port        = 0;
	error_                = 0;
}

Socket::~Socket()
{
	shutdown(sock_, SD_BOTH);
	closesocket(sock_);
}



// 设置socket
void Socket::socket(const SOCKET sock)
{
	sock_ = sock;
}

// 获取socket
SOCKET Socket::socket() const
{
	return sock_;
}


// 设置IP
void Socket::ip(const string& ip)
{
	addr_.sin_addr.s_addr = inet_addr(ip.c_str());
}

// 获取IP
string Socket::ip() const
{
	return inet_ntoa(((struct sockaddr_in)addr_).sin_addr);
}


// 设置port
void Socket::port(const ushort port)
{
	addr_.sin_port = htons(port);
}

// 获取port
ushort Socket::port() const
{
	return ntohs(((struct sockaddr_in)addr_).sin_port);
}



// 发送数据
// 参数:
//   buf: 发送数据的缓冲区
// 返回值:
//   false: 成功
//   true : 失败
// 第一个数据包结构为 sizeof(uint)为要传输的数据总长度 + socket_mss-sizeof(uint), 之后
bool Socket::send(string& buf)
{
	uint	sendLen  = 0;                             // 已发送的数据长度
	auto	dataLen  = buf.size();                    // 数据段总长度
	uint	totalLen = sizeof(dataLen) + buf.size();  // 总长度
	char* szBuf    = new char[totalLen];            // 要发送的数据

	memcpy(szBuf, &dataLen, sizeof(dataLen));
	memcpy(szBuf, &buf.front(), sizeof(buf.size()));

	if (totalLen <= socket_mss)
	{
		delete[] szBuf;
		return false;
	}

	// 数据长度大于MSS, 分片传输
	do
	{
		auto len = ::send(sock_, &szBuf[sendLen], socket_mss, 0);

		if (len == 0)	// 连接断开
		{
			delete[] szBuf;
			return true;
		}

		if (len < socket_mss)	// 发送不完整
		{
			delete[] szBuf;
			return true;
		}

		sendLen += len;
	} while (sendLen == totalLen);

	delete[] szBuf;

	return false;
}

// 接收数据
// 参数:
//   buf: 接收数据的缓冲区
// 返回值:
//   false: 成功
//   true : 失败
bool Socket::recv(string& buf)
{
	uint recvLen = 0; // 已接受的数据段总长度
	uint dataLen;     // 数据段总长度

	// 拆分第一个数据包
	char* szFirstBuf = new char[socket_mss];
	auto	len				 = ::recv(sock_, szFirstBuf, socket_mss, 0);

	if (len == 0)	// 连接断开
	{
		delete[] szFirstBuf;
		return true;
	}

	// FIXME: recvLen为非负整数, 但recv的返回值可能是-1
	if (len == SOCKET_ERROR)
	{
		delete[] szFirstBuf;
		return true;
	}

	recvLen -= sizeof(dataLen);
	memcpy(&dataLen, szFirstBuf, sizeof(dataLen));	// 提取数据段总长度

	char* szBuf = new char[dataLen];
	memcpy(szBuf, &szFirstBuf[sizeof(dataLen)], recvLen);
	delete[] szFirstBuf;

	// 数据长度大于MSS, 分片接收
	do
	{
		len = ::recv(sock_, &szBuf[recvLen], socket_mss, 0);

		if (len == 0)	// 连接断开
		{
			delete[] szBuf;
			true;
		}

		recvLen += len;
	} while (recvLen >= dataLen);

	memcpy(&buf.front(), szBuf, sizeof(szBuf));

	delete[] szBuf;

	return false;
}


// 请求连接
// 返回值:
//   false: 成功
//   true : 失败
bool Socket::connect()
{
	auto ret = ::connect(sock_, (sockaddr*)&addr_, sizeof(sockaddr));

	if (ret == SOCKET_ERROR)
		return true;

	return false;
}

// 请求连接
// 参数:
//   ip  : IPv4地址
//   port: 端口
// 返回值:
//   false: 成功
//   true : 失败
bool Socket::connect(string& ip, ushort port)
{
	addr_.sin_addr.s_addr = inet_addr(ip.c_str());
	addr_.sin_port				= htons(port);

	auto ret = ::connect(sock_, (sockaddr*)&addr_, sizeof(sockaddr));

	if (ret == SOCKET_ERROR)
		return true;

	return false;
}



// 绑定
// 参数:
//   port: 端口
// 返回值:
//   false: 成功
//   true : 失败
bool Socket::bind(ushort port)
{
	if (addr_.sin_addr.s_addr == 0)				// 没有IP
		addr_.sin_addr.s_addr = INADDR_ANY;	// 默认全部IP

	addr_.sin_port = htons(port);

	auto ret = ::bind(sock_, (sockaddr*)&addr_, sizeof(sockaddr));

	if (ret == SOCKET_ERROR)
		return true;

	return false;
}

// 绑定
// 参数:
//   ip  : IPv4地址
//   port: 端口
// 返回值:
//   false: 成功
//   true : 失败
bool Socket::bind(string& ip, ushort port)
{
	addr_.sin_addr.s_addr = inet_addr(ip.c_str());
	addr_.sin_port				= htons(port);

	auto ret = ::bind(sock_, (sockaddr*)&addr_, sizeof(sockaddr));

	if (ret == SOCKET_ERROR)
		return true;

	return false;
}



// 监听
// 挂起的连接队列的最大长度: 合理的最大值
// 返回值:
//   false: 成功
//   true : 失败
bool Socket::listen()
{
	auto ret = ::listen(sock_, SOMAXCONN);

	if (ret == SOCKET_ERROR)
		return true;

	return false;
}

// 监听
// 参数:
//   logback: 挂起的连接队列的最大长度
// 返回值:
//   false: 成功
//   true : 失败
bool Socket::listen(ushort logback)
{
	if (logback > SOMAXCONN)
		return true;

	auto ret = ::listen(sock_, logback);

	if (ret == SOCKET_ERROR)
		return true;

	return false;
}


// 接受连接请求
// 参数:
//   sock: socket
//   addr: sockaddr
// 返回值:
//   false: 成功
//   true : 失败
bool Socket::accept(Socket& sock)
{
	int addrlen = sizeof(sock.addr_);

	auto ret = ::accept(sock_, (sockaddr*)&sock.addr_, &addrlen);

	if (ret == INVALID_SOCKET)
		return false;

	sock.socket(ret);

	return false;
}


uint Socket::size_ = 0;

#undef buffer_size
#undef buffer_file_size
