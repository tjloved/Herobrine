// Copyright 2019 SMS
// License(GPL)
// Author: ShenMian
// 被控端

#include "socket.h"
#include "type.h"

#ifndef SLAVE_H_
#define SLAVE_H_


class Slave : public Socket
{
public:
	Slave();
	~Slave();

	uint	 id();
	uint	 lastId();

	void	 os_info(string&);
	ushort os_lang();
	ushort os_mem_total();
	ushort os_mem_free();
	ushort os_mem_available();

	static uint size();

private:
	uint			id_;
	char			os_info_[os_info_size];
	user_info user_;

	static uint	lastId_;	// 最新会话ID
	static uint	size_;		// 实例总数
};


#endif	// SLAVE_H_
