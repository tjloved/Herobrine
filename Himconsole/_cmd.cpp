// Copyright 2019 SMS
// License(GPL)
// Author: ShenMian

#include <conio.h>
#include <windows.h>
#include "cmd.h"
#include "print.h"
#include "type.h"


namespace cmd
{
typedef void (*func)(vector<string>&);

struct cmd_info_
{
	string name;
	string desc;

	func exec;
};
vector<struct cmd_info_> list;	// 命令列表

/*
const char* list[] = {"cls",		"clear", "exit", "help", "history",
											"listen", "ls",		 "quit", "use"};	// 命令列表
*/

const auto LIST_NULL = -1;

constexpr ushort list_size		= sizeof(list) / sizeof(char*);	// 命令数
constexpr ushort history_size = 10;	// 命令历史记录数
constexpr char	 prompt[]			= "him";

deque<string>	history(history_size);	// 命令历史记录
vector<string> arg;										 // 命令参数



// 命令行
void console()
{
	struct cmd_info_ tmp;
	tmp.name = "help";
	list.push_back(tmp);
	tmp.name = "exit";
	list.push_back(tmp);
	tmp.name = "quit";
	list.push_back(tmp);
	tmp.name = "ls";
	list.push_back(tmp);
	tmp.name = "listen";
	list.push_back(tmp);
	tmp.name = "use";
	list.push_back(tmp);
	tmp.name = "banner";
	list.push_back(tmp);

	char	 buf;	// 输入缓冲区
	string line;

	while (true)
	{
		// 输出命令提示符
		printf("\n");
		color::mode::underline();
		printf("him");
		color::rest();
		printf("> ");

		short pList = LIST_NULL;

		while (true)
		{
			color::fore::green();
			color::mode::bold();

			buf = _getch();

			color::rest();

			// 边界检查
			if (line.size() + 1 > line.max_size())
			{
				print::err("命令过长");
				line = "";
				break;
			}

			// 功能键
			if (buf == -32 || buf == 0)
			{
				buf = _getch();	// 获取功能键ASCII代码

				// 上/下键 切换命令历史记录
				if (buf == 72)	// 上键
				{
					;
				}

				if (buf == 80)	// 下键
				{
					;
				}

				// 切换到指定的命令历史记录
				if (buf == 72 || buf == 80)
				{
					;
				}

				continue;
			}

			// Enter键 结束输入
			if (buf == '\r')
			{
				printf("\n");
				break;
			}

			// Tab键 补全命令
			if (buf == '\t')
			{
				if (pList == LIST_NULL)	// 没有匹配项
					continue;
				printf("%s", &list[pList].name[line.size()]);
				line += &list[pList].name[line.size()];
				continue;
			}

			// ESC键 清空输入
			if (buf == 27)
			{
				for (auto i = 0; i < line.size(); i++)
					printf("\b \b");
				line.clear();
				continue;
			}

			// 过滤非法字符
			if (false)
				;

			// Backspace键 删除上一个字符
			if (buf == '\b')
			{
				if (line.size() <= 0)
					continue;
				printf("\b \b");
				line.pop_back();
			}
			else
			{
				// 回显输入的字符
				printf("%c", buf);
				line += buf;
			}

			// 输出预测提示
			for (auto i = 0; i < list.size(); i++)
			{
				if (list[i].name.compare(0, line.size(), line) == 0 &&
						!line.empty())	// 匹配成功
				{
					if (pList != LIST_NULL)	// 存在提示
					{
						if (i != pList)	// 擦除已有的提示
						{
							for (auto j = 0; j < list[pList].name.size(); j++)
								printf(" ");
							for (auto j = 0; j < list[pList].name.size(); j++)
								printf("\b");
						}
					}

					pList = i;

					color::fore::gray();

					// 输出提示
					printf("%s", &list[pList].name[line.size()]);

					color::fore::green();
					color::mode::bold();

					// 光标回退
					for (auto j = 0; j < list[pList].name.size() - line.size(); j++)
						printf("\b");

					break;
				}

				if (i == list.size() - 1)	// 匹配失败
				{
					if (pList != LIST_NULL)	// 存在提示
					{
						// 擦除提示
						for (auto j = 0; j < list[pList].name.size(); j++)
							printf(" ");
						for (auto j = 0; j < list[pList].name.size(); j++)
							printf("\b");
					}

					pList = LIST_NULL;
				}
			}
		}

		if (line.size() == 0)
			continue;

		// 拆分命令

		// 添加命令历史记录
		while (history.size() >= history_size)
			history.pop_front();
		history.push_back(line);

		line.clear();

		// 执行内部命令

		// 执行外部Module
	}
}



// 输出横幅
void banner()
{
	printf(
			"  __      __    __\n"
			" |  |    |  |  |__|   __________\n"
			" |  |____|  |   __   |  __  __  |\n"
			" |   ____   |  |  |  |  ||  ||  |\n"
			" |  |    |  |  |  |  |  ||  ||  |\n"
			" |__|    |__|  |__|  |__||__||__|\n"
			"\n"
			" [Herobrine (Alpha)             ]\n"
			" [Update Date: %-17.17s]\n"
			" [Update Time: %-17.17s]\n",
			__DATE__, __TIME__);
}

}	// namespace cmd

